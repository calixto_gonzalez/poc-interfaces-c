using System;
using System.Runtime.Serialization;
using System.Text;

namespace Eje53
{
  public class Pencil : IActions
  {
    private float sizeOfPencilLead;


    ConsoleColor IActions.InkColor { get; set; }

    float IActions.AmountOfInk
    {
      get { return sizeOfPencilLead; }
      set { throw new NotImplementedException(); }
    }

    public bool Recargar(int amount)
    {
      throw new NotImplementedException("Sarasaaaa");
    }

    public WritterWrapper Escribir(string text)
    {
      if (ReferenceEquals(text, null))
      {
        throw new Exception("text is null.");

      }

      StringBuilder stringToReturn = new StringBuilder();
      foreach (char c in text)
      {
        stringToReturn.Append(c);
        if (sizeOfPencilLead>0)
        {
          sizeOfPencilLead -= (float) 0.1;
        }
        else
        {
          return new WritterWrapper(stringToReturn.ToString(), ConsoleColor.Gray);
        }
      }

      return null;

    }
  }
}
