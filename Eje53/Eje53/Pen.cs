using System;

namespace Eje53
{
  public class Pen : IActions
  {
    private ConsoleColor inkColor;
    private float ink;

    public ConsoleColor InkColor { get; set; }
    public float AmountOfInk { get; set; }
  }
}
