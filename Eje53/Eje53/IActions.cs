using System;

namespace Eje53
{
  public interface IActions
  {
    ConsoleColor InkColor { get; set; }
    float AmountOfInk { get; set; }

  }
}
